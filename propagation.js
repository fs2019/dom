function stopEvent(ev) {
  const c2 = this
  c2.innerHTML = "I keep things private !"

  // this ought to keep table#top from getting the click.
  ev.stopPropagation()
}

function load() {
  const elem = document.getElementById("c2")
  elem.addEventListener("click", stopEvent, false)
}
