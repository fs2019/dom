#+Title: Événements dans un navigateur
#+Author: Pierre Gambarotto
#+Email: pierre.gambarotto@enseeiht.fr
#+OPTIONS: num:nil reveal_title_slide:auto toc:nil
#+OPTIONS: reveal_center:nil
#+REVEAL_THEME: sky
#+REVEAL_PLUGINS: (markdown notes zoom)
#+REVEAL_EXTRA_CSS: ./local.css
#+REVEAL_MARGIN: 0.01
#+REVEAL_ROOT: http://cdn.jsdelivr.net/reveal.js/3.0.0/

# C-c C-e R B|R to export as a reveal js presentation
#+LATEX_CLASS: article
#+LaTeX_CLASS_OPTIONS: [a4paper]
#+LaTeX_CLASS_OPTIONS: [12pt]
#+LaTeX_CLASS_OPTIONS: [listings, listings-sv]
#+LANGUAGE: fr
#+LATEX_HEADER: \usepackage[francais]{babel}

# C-c C-e l l/p/o to export as latex/pdf document

# tangle a block : C-u C-c C-v C-t (C-ucvt)


* DOM & Events

La partie interactive d'une page HTML est gérée de manière asynchrone

1. on réagit à des événements
2. en positionnant des callbacks

Les nœuds DOM implémentent l'interface [[https://developer.mozilla.org/en-US/docs/Web/API/EventTarget][EventTarget]]

#+BEGIN_SRC javascript
// element < eventTarget
let el = document.getElementById('toto')
el.addEventListener(type, callback)
// type : chaîne de caractères identifiant l'événement
// callback : fonction appelée pour gérer l'événement
// callback appelée avec un objet représentant l'événement
#+END_SRC

** Événements

- mouse : =click, dblclick, mouseup, mousedown=
- keyboard : =keydown, keyup=
- focus : =focus, focusin, blur, focusout=
- forms : =submit= quand on clicke sur un ~<input type="submit"/>~ dans un ~<form>~
- forms element : =input, textarea=
- window.DOMContentLoaded : DOM utilisable
- window.load : page chargée

[[https://developer.mozilla.org/fr/docs/Web/Events][Liste complète]]

#+REVEAL: split
À chaque événement est associé un objet le représentant.

C'est cet objet qui sera donné en argument de la fonction de callback de =addEventListener=.

Exemple pour [[https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent][MouseEvent]]

** Associer un événement à un tag

- inline (vieux)

#+BEGIN_SRC html
<a id="theLink" href="anyhere.html" onClick="doSomething()">
#+END_SRC

- modèle traditionnel:

#+BEGIN_SRC html
<script>
let element = document.getElementById('theLink')
element.onClick = doSomething
</script>
#+END_SRC

~onClick~ ne permet de référencer qu'un seul handler  

#+reveal: split
- JS moderne

#+BEGIN_SRC html
<script>
element.addEventListener('click',doSomething)
element.addEventListener('click',doAnother)
</script>
#+END_SRC

=> permet d'associer plusieurs handlers au même événement

=> permet d'annuler l'association avec [[https://developer.mozilla.org/fr/docs/Web/API/EventTarget/removeEventListener][removeEventListener]]

** Déclencher un événement avec du code

#+BEGIN_SRC javascript
const ev = new Event('toto')
element.dispatchEvent(ev) // synchronous call !
#+END_SRC

** Exemple: simuler un click souris
#+begin_src javascript
let b = document.createElement('button')
b.innerHTML = 'ok'
b.addEventListener('click', e => console.log('ok'))
document.body.appendChild(b)

// simulate a click
let ev = new MouseEvent('click')
b.dispatchEvent(ev)
#+end_src


* COMMENT Cycle des événement lors d'un chargement

1. ~[[https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded][DOMContentLoaded]]~ : DOM chargé
2. ~[[https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded][load]] : ressources annexes chargées (CSS, images …)

#+reveal: spilt
#+BEGIN_SRC html :tangle load_cycle.html
<html>
  <head>
    <meta charset="UTF-8">
    <script>
window.addEventListener('DOMContentLoaded', function() {
  console.log('window - DOMContentLoaded - capture'); // 1st
}, true);
document.addEventListener('DOMContentLoaded', function() {
  console.log('document - DOMContentLoaded - capture'); // 2nd
}, true);
document.addEventListener('DOMContentLoaded', function() {
  console.log('document - DOMContentLoaded - bubble'); // 2nd
});
window.addEventListener('DOMContentLoaded', function() {
  console.log('window - DOMContentLoaded - bubble'); // 3rd
});

window.addEventListener('load', function() {
  console.log('window - load - capture'); // 4th
}, true);
document.addEventListener('load', function(e) {
  /* Filter out load events not related to the document */
  if(['style','script'].indexOf(e.target.tagName.toLowerCase()) < 0)
    console.log('document - load - capture'); // DOES NOT HAPPEN
}, true);
document.addEventListener('load', function() {
  console.log('document - load - bubble'); // DOES NOT HAPPEN
});
window.addEventListener('load', function() {
  console.log('window - load - bubble'); // 4th
});

window.onload = function() {
  console.log('window - onload'); // 4th
};
document.onload = function() {
  console.log('document - onload'); // DOES NOT HAPPEN
};
</script>
</head>
</html>
#+END_SRC

* Cycle de vie d'un document HTML

1. chargement
2. analyse
3. construction du DOM -> ~DOMContentLoaded~
4. chargement des ressources annexes
5. affichage
6. traitement des événements ultérieurs

Une balise ~script~ inline ou synchrone rencontrée dans l'analyse l'interrompt.

** Attendre que le DOM ait été construit

Basique:
#+BEGIN_SRC javascript
document.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");
  })
#+END_SRC

Optimisé, avec [[https://developer.mozilla.org/fr/docs/Web/API/Document/readyState][document.readyState]]: 
#+BEGIN_SRC javascript
// `DOMContentLoaded` may fire before your script has a chance to run, so check before adding a listener
if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", doSomething)
} else {  // `DOMContentLoaded` already fired
    doSomething()
}
#+END_SRC
* Event, this

Que référence ~this~ dans le code d'un handler ?

Cela dépend de la manière dont le handler a été défini:

#+BEGIN_SRC javascript
element.addEventListener('click', doTheStuff)
// or
element.onclick = doTheStuff

function doTheStuff(){
// this => element
}
#+END_SRC

#+BEGIN_SRC html
<el id="e" onClick="doTheStuff()"/>

<script>
function doTheStuff{
// this => window
}
</script>
#+END_SRC


* Event, comportement par défaut

- comportement par défaut pour certaines combinaisons tag/événement.

Par exemple, le click sur un tag ~<a>~ génère une requête ~GET~ en utilisant la
valeur de l'attribut ~href~.

#+reveal: split
- [[https://developer.mozilla.org/fr/docs/Web/API/Event/preventDefault][e.preventDefault()]] : dans le handler d'un événement, cet appel permet de ne
  pas exécuter le comportement par défaut.

#+BEGIN_SRC html :tangle preventdefault.html
<a id="e" href="kowabunga">link</a>

<script>
var e = document.getElementById('e')
e.addEventListener('click', showDontLink)

function showDontLink(event){
  event.preventDefault()
  alert(this.getAttribute('href'))
}
</script>
#+END_SRC
* Flux d'événement

- même événement capturé par plusieurs éléments imbriqués
- que se passe-t-il ?


** The Mozilla Way


#+BEGIN_SRC plantuml :file i/event_capturing.png
@startditaa(--round-corners)

                |
+---------------|-----------------+
| element1      |                 |
|   +-----------|-----------+     |
|   |element2   v           |     |
|   +-----------------------+     |
|        Event CAPTURING          |
+---------------------------------+
@endditaa
#+END_SRC

** The Microsoft way 

#+BEGIN_SRC plantuml :file i/event_bubbling.png :cmdline -r
@startditaa
                ^
+---------------|-----------------+
| element1      |                 |
|   +-----------|-----------+     |
|   |element2   |           |     |
|   +-----------------------+     |
|        Event BUBBLING           |
+---------------------------------+
@endditaa
#+END_SRC

** The standard way

#+BEGIN_SRC plantuml :file i/event_w3c.png :cmdline -r
@startditaa
                |  ^
+---------------|--|--------------+
| element1      |  |              |
|   +-----------|--|--------+     |
|   |element2   v  |        |     |
|   +-----------------------+     |
|        W3C Event Model          |
+---------------------------------+

capture then bubbling
@endditaa
#+END_SRC

** Comment choisir ?

- ordre : capture puis bubble

[[https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener][target.addEventListener(type, listener{, useCapture});]]

- par défaut, événement traité en phase BUBBLING
- 3e argument à ~true~ pour traiter en CAPTURE

** Stopper la propagation

[[https://developer.mozilla.org/fr/docs/Web/API/Event/stopPropagation][event.stopPropagation()]]

- dans un handler
- stoppe la propagation de l'événement

** Example  


#+BEGIN_SRC html :tangle propagation.html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Event Propagation</title>
    <script src="propagation.js"></script>
    <style>
      #top { border: 1px solid red }
      #c1 { background-color: pink; }
    </style>
  </head>

  <body onload="load();">

  <table id="top" onclick="alert('table says hi')">
    <tr id="row">
      <td id="c1">one: click goes up to table</td>
    </tr>
    <tr>
      <td id="c2">two: click is not propagated</td>
    </tr>
  </table>
  </body>
</html>
#+END_SRC

#+reveal: split
#+BEGIN_SRC javascript :tangle propagation.js
function stopEvent(ev) {
  const c2 = this
  c2.innerHTML = "I keep things private !"

  // this ought to keep table#top from getting the click.
  ev.stopPropagation()
}

function load() {
  const elem = document.getElementById("c2")
  elem.addEventListener("click", stopEvent, false)
}
#+END_SRC

#+REVEAL: split

#+ATTR_HTML: :width 50% :height 50%
[[file:i/eventflow.svg]]


* Event: délégation

- Flux d'événement: capture, /target/, bubble
- déléguer la gestion d'événement: traiter hors de la phase target
- handler positionné sur un élément HTML englobant
- [[https://developer.mozilla.org/en-US/docs/Web/API/Event/target][~event.target~]] désigne alors l'élément qui a créé l'événement

** Exemple

#+BEGIN_SRC html :tangle /tmp/list.html
<ul id="parent-list">
	<li id="post-1">Item 1</li>
	<li id="post-2">Item 2</li>
	<li id="post-3" class="strange">Item 3</li>
	<li id="post-4">Item 4</li>
	<li id="post-5" class="strange">Item 5</li>
	<li id="post-6">Item 6</li>
</ul>
#+END_SRC

#+reveal: split

#+BEGIN_SRC javascript 
// Get the element, add a click listener...
document.getElementById("parent-list")
        .addEventListener("click", function(e) {
	// e.target is the clicked element!
	// If it was a list item
	if(e.target && e.target.nodeName == "LI") 
  // List item found!  Output the ID!
		console.log("List item ", 
                e.target.id.replace("post-", ""), 
                " was clicked!")
})
#+END_SRC

** Utilité et outils

- factoriser la gestion d'un événement commun à plusieurs éléments
- nécessite de filtrer selon ~event.target~
- [[https://developer.mozilla.org/fr/docs/Web/API/Element/matches][Element.matches(selectorString)]] : teste si un élément correspond à un filtre
  (syntaxe CSS comme ~document.querySelector[All]()~

#+BEGIN_SRC javascript
// Get the element, add a click listener...
document.getElementById("parent-list")
        .addEventListener("click", function(e) {
	// If e.target is a list item with class="strange"
	if(e.target.matches('li.strange')) 
    // Strange List item found!  Output the ID!
		console.log("List item ", 
                e.target.id.replace("post-", ""), 
                " was clicked!")
})
#+END_SRC
- [[https://developer.mozilla.org/fr/docs/Web/API/Element/closest][Element.closest(selectorString)]] : renvoie le plus proche parent vérifiant le
  filtre css.
