* Délégation d'événements

** Pratiquons un peu git …
Clonez le dépôt [[https://gitlab.com/fs2019/aliments][gitlab.com/fs2019/aliments]] sur votre ordinateur.

#+begin_src shell 
git clone https://gitlab.com/fs2019/aliments
#+end_src
Créer et basculer sur une branche ~tp_events_delegation~:

#+begin_src shell
cd aliments
git checkout -b tp_events_delegation
#+end_src

** Codez la fonctionnalité: click réduit l'affichage des nutriments
un click sur la liste des nutriments d'un plat ou sur le titre d'un plat va
rendre la liste des nutriments
invisible, et uniquement celle là.

Rappel de la méthode: 
1. Déterminez l'événement et le tag html visé. Testez le déclenchement avec un
   ~console.log(…)~ dans le navigateur sur un élément exemple.
2. Rajouter le comportement visé dans votre test.
3. Codez la version définitive pour l'élément exemple.
4. Généralisez à tous les éléments visés. Testez. (map …)
5. Basculez le code dans un fichier, testez.

Ici:
1. événement: click, tags html concernés:  ~dl~ dans un ~li~, et le ~li~ englobant. Prenez un des
   ~li~, récupérer une représentation, et faites les essais dessus.
2. comportement visé: élévemt visé qui devient caché.
3. le click sur la ~dl~ ou sur le ~li~  de l'exemple doit rendre la ~dl~ invisible.
4. le click sur une ~dl~/un ~li~ quelconque doit rendre invisible la ~dl~.
5. Recharchez la page, tout doit marcher !

** Délégation d'événement
Dans l'exercice précédent, nous avons ajouté un événement sur chaque ~dl~
présent dans notre liste.

Utilisons plutôt la propagation d'événement, en réalisant la chose suivante:

Nous allons positionner un événement ~click~ et son callback associé sur l'élément
  ~ul~ englobant.

Si je clique sur un des éléments ~li~, qu'est-ce que le callback positionné sur le ~ul~
peut faire ? 

Quelle est la phase de propagation concernée ?

*** Testez en console
Pour vous convaincre que vous avez saisi le fonctionnement, réalisez un callback
associé à l'événement ~click~ sur le tag ~ul~ qui affiche dans la console:
- l'élément cible de l'événement
- le nom de l'aliment sur lequel le clic se produit.

Hint: [[https://developer.mozilla.org/fr/docs/Web/API/Event/target][Event.target]]

Hint: [[https://developer.mozilla.org/fr/docs/Web/API/Element/closest][Element.closest()]]

*** Un seul callback: click réduit l'affichage des nutriments
Positionnez un seul callback sur le ~ul~ qui réalise la même chose que
précédemment: un click sur la liste des nutriments d'un plat va rendre la liste
invisible, et uniquement celle là.
